package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	cardList := createDeck()
	fmt.Println("Card Deck:")
	fmt.Println(cardList)
	shuffList := shuffle(cardList)
	fmt.Println("Shuffled Card Deck:")
	fmt.Println(shuffList)
	fmt.Println(len(shuffList))
	card, newList := deal(shuffList)
	fmt.Println("Card:")
	fmt.Println(card)
	fmt.Println("Card Deck left:")
	fmt.Println(newList)
	fmt.Println(len(newList))
	shuffList2 := shuffle(newList)
	fmt.Println("Shuffled Card Deck:")
	fmt.Println(shuffList2)
}

func getRand(floor int, ceiling int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(ceiling-floor) + floor
}

func shuffle(cardList []string) []string {
	lenList := len(cardList)
	if lenList <= 1 {
		return cardList
	}

	for i := 0; i < lenList; i++ {
		swapIdx := getRand(i, lenList)
		cardList[i], cardList[swapIdx] = cardList[swapIdx], cardList[i]
	}
	return cardList
}

func deal(cardList []string) (string, []string) {
	card := cardList[0]
	cardList = cardList[1:len(cardList)]
	return card, cardList
}

func createDeck() []string {
	var cardList []string
	suitArr := [4]string{"\u2663", "\u2662", "\u2661", "\u2660"}
	cardArr := [13]string{"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"}
	for _, element := range suitArr {
		for _, val := range cardArr {
			cardList = append(cardList, val+element)
		}
	}
	return cardList
}
